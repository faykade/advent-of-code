function isPrime(num) {
  for (let i = 2; i <= Math.sqrt(num); i++) {
    if (num % i === 0) return false; // If the number is divisible by any number other than 1 and itself, it's not a prime
  }
  return true; // Return true if the number is prime
}

// Function to find prime factors of a number
function getPrimeFactorization(num) {
  // Function to check if a number is prime
  

  const result = []; // Initialize an empty array to store prime factors
  
  // Loop through numbers from 2 to 'num'
  for (let i = 2; i <= num; i++) {
    // While 'i' is a prime factor and divides 'num' evenly
    while (isPrime(i) && num % i === 0) {
      result.push(i); // Add 'i' to the result array if it's not already present
      num = num / i; // Divide 'num' by 'i'
    }
  }
  
  return result; // Return the array containing prime factors
}

module.exports = getPrimeFactorization;