const getPrimeFactorization = require('./getPrimeFactorization');

function getLCM(...numbers){
  const numCounts = {};
  const factorizations = numbers.map(getPrimeFactorization);
  factorizations.forEach(factorization => {
    const currObj = factorization.reduce((acc, curr) => ({
      ...acc,
      [curr]: acc[curr] ? acc[curr] + 1 : 1
    }), {});
    Object.keys(currObj).forEach(key => {
      numCounts[key] = Math.max(numCounts[key] || 1, currObj[key])
    })
  })
  return Object.keys(numCounts).reduce((acc, curr) => acc * Math.pow(parseInt(curr), numCounts[curr]), 1)
}

module.exports = getLCM;