const getPrimeFactorization = require("./getPrimeFactorization");
const getLCM = require("./getLCM");

module.exports = {
  getPrimeFactorization,
  getLCM
}