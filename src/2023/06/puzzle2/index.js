const fs = require('node:fs');

function getParsedLine(line){
  return [parseInt(line.split(':')[1].trim().replace(/\s+/g, ''))];
}

function getOrganizedData(lines){
  let raceTimes = getParsedLine(lines[0]);
  let records = getParsedLine(lines[1]);

  return raceTimes.map((raceTime, i) => ({
    raceTime,
    recordDistance: records[i]
  }))
}

function getProcessedRace(race){
  const attempts = [];
  for(let secondsHeld = 0; secondsHeld <= race.raceTime; secondsHeld++){
    const movingTime = race.raceTime - secondsHeld;
    const distanceMoved = secondsHeld * movingTime;
    attempts.push({
      movingTime,
      distanceMoved
    })
  }
  const wins = attempts.filter(attempt => attempt.distanceMoved > race.recordDistance);

  return {
    race,
    attempts,
    wins
  };
}

function getProcessedData(organizedData){
  return organizedData.map(getProcessedRace);
}

function getFinalAnswer(processedRaces) {
  return processedRaces.map(race => race.wins.length).reduce((acc, winCount) => acc * winCount, 1);
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData', organizedData);

  const processedData = getProcessedData(organizedData);
  // console.log('processedData: ', JSON.stringify(processedData, undefined, 4));

  const finalAnswer = getFinalAnswer(processedData);
  console.log('finalAnswer: ', finalAnswer);
  
});