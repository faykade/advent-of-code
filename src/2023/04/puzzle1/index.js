const fs = require('node:fs');

function getArrayOfNumbers(data){
  return data.trim().split(' ').filter(item => item).map(val => parseInt(val));
}

function getParsedLine(line, index) {
  const [card, data] = line.split(':');
  const [winners, myNumbers] = data.split('|');
  const numArrWinners = getArrayOfNumbers(winners)
  const numArrMyNumbers = getArrayOfNumbers(myNumbers)
  const myWinners = numArrWinners.filter(winner => numArrMyNumbers.includes(winner));
  return {
    card: index + 1,
    index,
    winners: numArrWinners,
    myNumbers: numArrMyNumbers,
    myWinners,
    winningValue: myWinners.length > 0 ? Math.pow(2, myWinners.length - 1) : 0
  }
}

function getOrganizedData(lines){
  return lines.map(getParsedLine)
}

function getFinalAnswer(organizedData){
  return organizedData.reduce((acc, curr) => acc + curr.winningValue, 0)
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', JSON.stringify(organizedData, undefined, 4));

  const finalAnswer = getFinalAnswer(organizedData);
  console.log('final answer: ', finalAnswer);
  
});