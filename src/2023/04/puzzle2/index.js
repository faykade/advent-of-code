const fs = require('node:fs');

function getArrayOfNumbers(data){
  return data.trim().split(' ').filter(item => item).map(val => parseInt(val));
}

function getParsedLine(line, index) {
  const [card, data] = line.split(':');
  const [winners, myNumbers] = data.split('|');
  const numArrWinners = getArrayOfNumbers(winners)
  const numArrMyNumbers = getArrayOfNumbers(myNumbers)
  const myWinners = numArrWinners.filter(winner => numArrMyNumbers.includes(winner));
  return {
    card: index + 1,
    index,
    winners: numArrWinners,
    myNumbers: numArrMyNumbers,
    myWinners,
  }
}

function getOrganizedData(lines){
  return lines.map(getParsedLine)
}

function getCopies(organizedData) {
  //initiate
  const copies = {};
  organizedData.forEach(card => {
    copies[card.index] = 1;
  })

  organizedData.forEach(card => {
    const numWinners = card.myWinners.length;
    const winningIndices = [];
    for(let i = 0; i < numWinners; i++){
      winningIndices.push(card.index + i + 1);
    }
    winningIndices.forEach(index => {
      copies[index] = copies[index] + copies[card.index];
    })
  })

  return copies;
}

function getFinalAnswer(copies){
  return Object.values(copies).reduce((acc, curr) => acc + curr, 0);
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', JSON.stringify(organizedData, undefined, 4));

  const copies = getCopies(organizedData);
  console.log('copies: ', copies);

  const finalAnswer = getFinalAnswer(copies);
  console.log('final answer: ', finalAnswer);
  
});