const fs = require('node:fs');

function getReplacement(textNumber) {
  switch(textNumber){
    case 'one':
      return '1';
    case 'two':
      return '2';
    case 'three':
      return '3';
    case 'four':
      return '4';
    case 'five':
      return '5';
    case 'six':
      return '6';
    case 'seven':
      return '7';
    case 'eight':
      return '8';
    case 'nine':
      return '9';
    default:
      return textNumber;
  }
}

function getNumericLine(line){
  let numberRegex = /[0-9]/g;
  return line.match(numberRegex).join('');
}

function getFirstLastNumbers(line){
  return `${line[0]}${line[line.length - 1]}`
}

function getInts(line){
  return parseInt(line);
}

function getBrokenDownData(lines){
  return lines.map(line => {
    const numericLine = getNumericLine(line);
    const firstLast = getFirstLastNumbers(numericLine);
    const finalNumber = getInts(firstLast);
    return {
      line,
      numericLine,
      firstLast,
      finalNumber
    }
  })
}

function getFinalAnswer(lines){
  return lines.reduce((acc, curr) => acc + curr.finalNumber, 0);
}


fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  const brokenDownData = getBrokenDownData(parsedData);
  console.log('Broken down data: ', brokenDownData);
  const finalAnswer = getFinalAnswer(brokenDownData);
  console.log('Final Answer: ', finalAnswer);
});