const fs = require('node:fs');

function getGrab(grabSection) {
  const colorGrabs = grabSection.split(',');
  // console.log('colorGrabs: ', colorGrabs);
  const ret = {};
  colorGrabs.forEach(colorGrab => {
    const [count, color] = colorGrab.trim().split(' ');
    ret[color] = parseInt(count);
  })
  return ret;
}

function getGrabs(line) {
  const grabsString = line.split(':')[1].trim();
  // console.log('grabsString: ', grabsString);
  return grabsString.split(';').map(getGrab);
}

function getMinimumCount(grabs, color){
  const colorGrabs = grabs.map(grab => grab[color] || 0);
  const minNeeded = colorGrabs.reduce((acc, curr) => acc > curr ? acc : curr, 0);
  return minNeeded;
}

function getMinimumCounts(grabs){
  return {
    red: getMinimumCount(grabs, 'red'),
    green: getMinimumCount(grabs, 'green'),
    blue: getMinimumCount(grabs, 'blue'),
  }
}

function getPowerOfCubes(minimums) {
  return minimums.red * minimums.blue * minimums.green;
}

function getOrganizedData(lines){
  return lines.map((line, i) => {
    const grabs = getGrabs(line);
    const minimums = getMinimumCounts(grabs)
    return {
      gameNumber: i + 1,
      grabs,
      minimums,
      powerOfCubes: getPowerOfCubes(minimums)
    }
  })
}

function getFinalAnswer(games) {
  return games.reduce((acc, game) => acc + game.powerOfCubes, 0)
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const games = getOrganizedData(parsedData);
  console.log('games: ', JSON.stringify(games, undefined, 4));

  const finalAnswer = getFinalAnswer(games);
  console.log('final answer: ', finalAnswer);
});