const fs = require('node:fs');

function getGrab(grabSection) {
  const colorGrabs = grabSection.split(',');
  // console.log('colorGrabs: ', colorGrabs);
  const ret = {};
  colorGrabs.forEach(colorGrab => {
    const [count, color] = colorGrab.trim().split(' ');
    ret[color] = parseInt(count);
  })
  return ret;
}

function getGrabs(line) {
  const grabsString = line.split(':')[1].trim();
  // console.log('grabsString: ', grabsString);
  return grabsString.split(';').map(getGrab);
}

function getOrganizedData(lines){
  return lines.map((line, i) => ({
    gameNumber: i + 1,
    grabs: getGrabs(line)
  }))
}

function isGrabPossible(grab){
  // console.log('grabred: ', grab['red'])
  const isRedPossible = grab['red'] ? grab['red'] <= 12 : true;
  const isGreenPossible = grab['green'] ? grab['green'] <= 13 : true;
  const isBluePossible = grab['blue'] ? grab['blue'] <= 14 : true;

  return isRedPossible && isGreenPossible && isBluePossible;
}

function isGamePossible(game){
  return game.grabs.filter(isGrabPossible).length === game.grabs.length;
}

function getSumOfGameNumbers(games) {
  return games.reduce((acc, curr) => acc + curr.gameNumber, 0);
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const games = getOrganizedData(parsedData);
  console.log('games: ', JSON.stringify(games, undefined, 4));

  const possibleGames = games.filter(isGamePossible);
  console.log('possibleGames: ', possibleGames);

  const finalAnswer = getSumOfGameNumbers(possibleGames);
  console.log('Final answer: ', finalAnswer)
});