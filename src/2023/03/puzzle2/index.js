const fs = require('node:fs');

function getNumbersFromLine(line){
  const regex = /[0-9]+/g;
  const foundValues = Array.from(line.matchAll(regex));

  const numbers = foundValues.reduce((acc, curr) => [...acc, {value: curr[0], index: curr.index}], []);

  return numbers;
}

function getAdjacentIndices(numberObj, lineNumber, lines) {
  console.log('numberObj: ', numberObj);
  console.log('line: ', lines[lineNumber]);
  console.log('lineNumber: ', lineNumber);
  const hasLineAbove = lineNumber > 0;
  const hasLineBelow = lineNumber < lines.length - 1;
  const hasLineLeft = numberObj.index > 0;
  const hasLineRight = numberObj.index + numberObj.value.length < lines[lineNumber].length;

  const startHorizontal = numberObj.index - (hasLineLeft ? 1 : 0);
  const endHorizontal = numberObj.index + numberObj.value.length - (hasLineRight ? 0 : 1);
  const startVertical = hasLineAbove ? lineNumber - 1 : lineNumber;
  const endVertical = hasLineBelow ? lineNumber + 1 : lineNumber;

  console.log('hasLineAbove: ', hasLineAbove);
  console.log('hasLineBelow: ', hasLineBelow);
  console.log('hasLineLeft: ', hasLineLeft);
  console.log('hasLineRight: ', hasLineRight);

  console.log('startHorizontal: ', startHorizontal);
  console.log('endHorizontal: ', endHorizontal);
  console.log('startVertical: ', startVertical);
  console.log('endVertical: ', endVertical);
  
  const adjacentIndices = [];

  if(hasLineAbove){
    const row = startVertical;
    for(let col = startHorizontal; col <= endHorizontal; col++){
      adjacentIndices.push([row, col])
    }
  }

  if(hasLineBelow){
    const row = endVertical;
    for(let col = startHorizontal; col <= endHorizontal; col++){
      adjacentIndices.push([row, col])
    }
  }

  if(hasLineLeft){
    adjacentIndices.push([lineNumber, startHorizontal]);
  }

  if(hasLineRight){
    adjacentIndices.push([lineNumber, endHorizontal])
  }

  return adjacentIndices;
}

function matchesIndicator(cell){
  console.log('cell: ', cell);
  const regex = /[^0-9\.]/

  // console.log('matches: ', regex.test(cell))
  return regex.test(cell);
}

function getNumberGearInfo(numberObj, lineNumber, lines) {
  const adjacentIndices = getAdjacentIndices(numberObj, lineNumber, lines);
  console.log('adjacentIndices: ', adjacentIndices);

  const ret = {};
  adjacentIndices.forEach(([row, col]) => {
    if(lines[row][col] === '*'){
      ret[`${row}-${col}`] = [parseInt(numberObj.value)]
    }
  })

  return ret;
}

function collateData(collector = {}, addition = {}){
  Object.keys(addition).forEach(key => {
    collector[key] = collector[key] ? [...collector[key], ...addition[key]] : [...addition[key]];
  })

  return collector;
}

function getLineGearInfo(numbers, lineNumber, lines) {
  let lineGears = {};
  numbers.forEach(numberObj => {
    const numberGears = getNumberGearInfo(numberObj, lineNumber, lines);
    lineGears = collateData(lineGears, numberGears);
  })

  return lineGears;
}

function getOrganizedData(lines){
  return lines.map((line, index) => {
    const numbers = getNumbersFromLine(line);
    const lineGearInfo = getLineGearInfo(numbers, index, lines);
    return {
      line,
      lineNumber: index,
      numbers,
      lineGearInfo
    }
  })
}

function getAllLineGears(organizedData) {
  let collector = {};
  const lineGearData = organizedData.map(line => line.lineGearInfo);
  lineGearData.forEach(lineGears => {
    collector = collateData(collector, lineGears)
  })
  return collector;
}

function getAppropriateGears(allLineGears){
  return Object.values(allLineGears).filter(gears => gears.length === 2);
}

function getFinalAnswer(appropriateGears){
  return appropriateGears.map(([val1, val2]) => val1 * val2).reduce((acc, curr) => acc + curr, 0);
}


fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', JSON.stringify(organizedData, undefined, 4));

  const allLineGears = getAllLineGears(organizedData);
  console.log('allLineGears: ', allLineGears);

  const appropriateGears = getAppropriateGears(allLineGears);
  console.log('appropriateGears: ', appropriateGears);

  const finalAnswer = getFinalAnswer(appropriateGears);
  console.log('final answer: ', finalAnswer);

});