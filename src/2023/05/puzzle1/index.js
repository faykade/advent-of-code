const fs = require('node:fs');

function getSeedSection(seedsSection) {
  return seedsSection.split(': ')[1].trim().split(' ').map(item => parseInt(item))
}

function getMapSection(mapSection) {
  console.log(mapSection);

  return {
    mapType: mapSection[0].split(' ')[0],
    values: mapSection.slice(1, mapSection.length).map(row => row.split(' ').map(item => parseInt(item))),
  }
}

function getMapping(input, map) {
  let ret = input;
  map.values.forEach(([destinationStart, sourceStart, range]) => {
    const sourceEnd = sourceStart + range - 1;
    if(input >= sourceStart && input <= sourceEnd){
      ret = destinationStart + (input - sourceStart);
    }
  })

  return ret;
}

function getSeedData(seed, maps){
  let ret = {seed};
  maps.forEach((map, index) => {
     ret[map.mapType] = getMapping(index === 0 ? seed : ret[maps[index - 1].mapType], map)
  })
  return ret;
}

function getOrganizedData(lines){
  const sections = [];

  for(let i = 0, startIndex = 0; i < lines.length; i++){
    const isLastLine = i + 1 === lines.length;
    if(!lines[i] || isLastLine){
      sections.push(lines.slice(startIndex, isLastLine ? lines.length : i))
      startIndex = i + 1;
    }
  }

  const [seedsSection, ...mapSections] = sections;

  const seeds = getSeedSection(seedsSection[0]);
  const maps = mapSections.map(getMapSection)

  const ret = {
    seeds: seeds.map(seed => getSeedData(seed, maps)),  
  };

  return ret;
}

function getFinalAnswer(organizedData) {
  return Math.min(...organizedData.seeds.map(seed => seed['humidity-to-location']));
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', JSON.stringify(organizedData, undefined, 4));

  const finalAnswer = getFinalAnswer(organizedData);
  console.log('final answer: ', finalAnswer);
});