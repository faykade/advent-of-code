const fs = require('node:fs');

function getSeedSection(seedsSection) {
  return seedsSection.split(': ')[1].trim().split(' ').map(item => parseInt(item))
}

function log(...args){
  // console.log(...args);
}

function alwaysLog(...args){
  console.log(...args);
}

function getMapSection(mapSection) {
  log(mapSection);

  return {
    mapType: mapSection[0].split(' ')[0],
    values: mapSection.slice(1, mapSection.length).map(row => row.split(' ').map(item => parseInt(item))),
  }
}

function getMapping(inputs, map) {
  const ret = [];
  let togo = inputs;
  log('map: ', map.mapType);
  log('inputs: ', JSON.stringify(togo, undefined, 4));
  map.values.forEach(([destinationStart, sourceStart, range], mapIndex) => {
    const sourceEnd = sourceStart + range - 1;
    const destinationEnd = destinationStart + range - 1;
    let tempTogo = [];
    log('range: ', range);
    log('sourceStart: ', sourceStart);
    log('sourceEnd: ', sourceEnd);
    log('destinationStart: ', destinationStart);
    log('destinationEnd: ', destinationEnd);
    togo.forEach(({seedStart, seedEnd}) => {
      log('seedStart: ', seedStart);
      log('seedEnd: ', seedEnd);
      // front enclosed
      if(seedStart >= sourceStart && seedStart <= sourceEnd){
        const containedRange = {
          seedStart: destinationStart + (seedStart - sourceStart), 
          seedEnd: Math.min(destinationEnd, destinationStart + (seedEnd - sourceStart))
        }
        ret.push(containedRange);
        log('front enclosed', containedRange)
        if(seedEnd > sourceEnd){
          const uncontainedRange = {
            seedStart: sourceEnd + 1,
            seedEnd
          }
          tempTogo.push(uncontainedRange);
        }
      // ending enclosed
      } else if(seedEnd >= sourceStart && seedEnd <= sourceEnd) {
        const containedRange = {
          seedStart: Math.max(destinationStart, destinationStart + (seedStart - sourceStart)), 
          seedEnd: destinationStart + (seedEnd - sourceStart)
        }
        ret.push(containedRange)
        log('end enclosed', containedRange);
        if(seedStart < sourceStart){
          const uncontainedRange = {
            seedStart,
            seedEnd: sourceStart - 1
          }
          tempTogo.push(uncontainedRange);
        }
      // overlaps whole range
      } else if(seedStart <= sourceStart && seedEnd >= sourceEnd) {
        const containedRange = {seedStart: destinationStart, seedEnd: destinationEnd};
        log('all enclosed', containedRange);
        ret.push(containedRange);
        if(seedStart < sourceStart){
          const uncontainedRange = {
            seedStart,
            seedEnd: sourceStart - 1
          }
          tempTogo.push(uncontainedRange);
        }
        if(seedEnd > sourceEnd){
          const uncontainedRange = {
            seedStart: sourceEnd + 1,
            seedEnd
          }
          tempTogo.push(uncontainedRange);
        }
      // no overlap
      } else {
        log('no enclosed');
        tempTogo.push({
          seedStart, 
          seedEnd
        })
      }
      togo = tempTogo;
      log('remaining: ', togo);
      if(mapIndex === map.values.length - 1 && togo.length){
        ret.push(...togo);
      }
    })
    log('----------------------------');
  })

  log('output: ', ret)
  log('=============================');

  return ret;
}

function getSeedData(seedStart, seedEnd, maps){
  let ret = {seedStart, seedEnd};
  maps.forEach((map, index) => {
     ret[map.mapType] = getMapping(index === 0 ? [{seedStart, seedEnd}] : ret[maps[index - 1].mapType], map)
  })
  return ret;
}

function getProcessedSeeds(seeds, maps){
  let seedResponses = [];
  for(let i = 0; i < seeds.length; i += 2){
    const start = seeds[i];
    const range = seeds[i + 1];
    const end = start + range - 1;
    alwaysLog('start-end: ', `${start}-${end}`)
    seedResponses.push(getSeedData(start, end, maps));
  }
  return seedResponses;
}

function getOrganizedData(lines){
  const sections = [];

  for(let i = 0, startIndex = 0; i < lines.length; i++){
    const isLastLine = i + 1 === lines.length;
    if(!lines[i] || isLastLine){
      sections.push(lines.slice(startIndex, isLastLine ? lines.length : i))
      startIndex = i + 1;
    }
  }

  const [seedsSection, ...mapSections] = sections;

  const seeds = getSeedSection(seedsSection[0]);
  const maps = mapSections.map(getMapSection)

  const ret = getProcessedSeeds(seeds, maps);

  return ret;
}

function getFinalAnswer(organizedData) {
  return Math.min(...organizedData.map(seed => seed['humidity-to-location']).flatMap(val => Math.min(...val.map(item => item.seedStart))));
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  alwaysLog('parsedData: ', parsedData);

  // const numbers = parsedData.filter(row => /^[0-9]/.test(row)).join(' ').split(' ').map(item => parseInt(item));
  // log('numbers: ', numbers);
  // const max = Math.max(...numbers);
  // log('max: ', max)
  // log('2max: ', max + max);

  const organizedData = getOrganizedData(parsedData);
  alwaysLog('organizedData: ', JSON.stringify(organizedData, undefined, 4));

  const finalAnswer = getFinalAnswer(organizedData);
  alwaysLog('final answer: ', finalAnswer);
});