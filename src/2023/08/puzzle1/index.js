const fs = require('node:fs');

function log(...args){
  console.log(...args);
}

function getOrganizedMap(mapLine){
  const [location, steps] = mapLine.split('=').map(val => val.trim());
  const [left, right] = steps.split(',').map(val => val.trim());
  return {
    location,
    L: left.slice(1, left.length),
    R: right.slice(0, right.length - 1)
  }
}

function getOrganizedData(parsedData) {
  const [directions, blank, ...maps] = parsedData;
  const organizedMaps = maps.map(getOrganizedMap);
  const ret = {
    directions,
    directionsArr: directions.split(''),
    maps: {}
  }
  organizedMaps.forEach(map => {
    ret.maps[map.location] = map
  })
  return ret;
}

function getFinalAnswer(processedData){
  let stepCount = 0;
  const start = 'AAA';
  const end = 'ZZZ';
  let found = false;
  let location = start;
  while(!found){
    if(location === end){
      found = true;
    } else {
      const directionsIndex = stepCount % processedData.directionsArr.length;
      const currentDirection = processedData.directionsArr[directionsIndex];
      location = processedData.maps[location][currentDirection];
      stepCount++;
    }
  }
  return stepCount;
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const finalAnswer = getFinalAnswer(organizedData);
  console.log('finalAnswer: ', finalAnswer);
});