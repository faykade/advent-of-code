const fs = require('node:fs');
const { getLCM, getPrimeFactorization } = require('../../../utils/functionality');

function log(...args){
  console.log(...args);
}

function getOrganizedMap(mapLine, index){
  const [location, steps] = mapLine.split('=').map(val => val.trim());
  const [left, right] = steps.split(',').map(val => val.trim());
  return {
    location,
    index,
    L: left.slice(1, left.length),
    R: right.slice(0, right.length - 1)
  }
}

function getOrganizedData(parsedData) {
  const [directions, blank, ...maps] = parsedData;
  const organizedMaps = maps.map(getOrganizedMap);
  const ret = {
    directions,
    directionsArr: directions.split(''),
    maps: {}
  }
  organizedMaps.forEach(map => {
    ret.maps[map.location] = map
  })
  return ret;
}

function isDone(currentLocation){
  // const endRegex = /.*Z$/
  const endRegex = /.*Z$/
  return endRegex.test(currentLocation);
}

function getStartingLocations(locations){
  const startRegex = /.*A$/;
  return locations.filter(location => startRegex.test(location));
}

function getNextLocation(currentLocation, maps, currentDirection){
  return maps[currentLocation][currentDirection];
}

function getPathLength(startingLocation, processedData){
  let stepCount = 0;
  let found = false;
  let location = startingLocation;
  while(!found){
    if(isDone(location)){
      found = true;
    } else {
      const directionsIndex = stepCount % processedData.directionsArr.length;
      const currentDirection = processedData.directionsArr[directionsIndex];
      location = getNextLocation(location, processedData.maps, currentDirection);
      stepCount++;
    }
  }
  return stepCount;
}

function getFinalAnswer(processedData){
  const starts = getStartingLocations(Object.keys(processedData.maps));
  const lengths = starts.map(location => getPathLength(location, processedData));
  log('lengths: ', lengths);
  return getLCM(...lengths);
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {

  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const finalAnswer = getFinalAnswer(organizedData);
  console.log('finalAnswer: ', finalAnswer);
});