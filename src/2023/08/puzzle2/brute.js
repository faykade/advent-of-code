const fs = require('node:fs');

function log(...args){
  console.log(...args);
}

function getOrganizedMap(mapLine, index){
  const [location, steps] = mapLine.split('=').map(val => val.trim());
  const [left, right] = steps.split(',').map(val => val.trim());
  return {
    location,
    index,
    L: left.slice(1, left.length),
    R: right.slice(0, right.length - 1)
  }
}

function getOrganizedData(parsedData) {
  const [directions, blank, ...maps] = parsedData;
  const organizedMaps = maps.map(getOrganizedMap);
  const ret = {
    directions,
    directionsArr: directions.split(''),
    maps: {}
  }
  organizedMaps.forEach(map => {
    ret.maps[map.location] = map
  })
  return ret;
}

function isDone(currentLocations){
  // const endRegex = /.*Z$/
  const endRegex = /.*Z$/
  const meetingEndCriteria = currentLocations.filter(location => endRegex.test(location))
  return meetingEndCriteria.length === currentLocations.length;
}

function getStartingLocations(locations){
  const startRegex = /.*A$/;
  return locations.filter(location => startRegex.test(location));
}

function getNextLocation(currentLocation, maps, currentDirection){
  return maps[currentLocation][currentDirection];
}

function getFinalAnswer(processedData){
  let stepCount = 0;
  const start = getStartingLocations(Object.keys(processedData.maps));
  let found = false;
  let locations = start;
  while(!found){
    if(stepCount % 10000000 === 0){
      log('stepCount: ', stepCount);
    }
    if(isDone(locations)){
      log('final locations: ', locations);
      found = true;
    } else {
      const directionsIndex = stepCount % processedData.directionsArr.length;
      const currentDirection = processedData.directionsArr[directionsIndex];
      locations = locations.map(location => getNextLocation(location, processedData.maps, currentDirection));
      stepCount++;
    }
  }
  return stepCount;
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const finalAnswer = getFinalAnswer(organizedData);
  console.log('finalAnswer: ', finalAnswer);
});