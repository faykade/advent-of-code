const fs = require('node:fs');

function log(...args){
  console.log(...args);
}

function getOrganizedData(parsedData) {
  return parsedData.map(line => line.split(' ').map(item => parseInt(item)));
}

function getNextRow(currentRow){
  const nextRow = [];
  for(let i = 0; i < currentRow.length - 1; i++){
    nextRow.push(currentRow[i + 1] - currentRow[i]);
  }

  return nextRow;
}

function getPredictedHistory(processedHistoryRow){
  // know the prediction when it is all 0s
  const predictions = [[...processedHistoryRow[processedHistoryRow.length - 1], 0]];
  let lastPrediction = 0;
  let currentRow;
  let currentPrediction;
  let predictedRow;
  for(let i = processedHistoryRow.length - 2; i >= 0; i--){
    currentRow = processedHistoryRow[i];
    currentPrediction = currentRow[currentRow.length - 1] + lastPrediction;
    predictedRow = [...currentRow, currentPrediction];
    // log('currentRow: ', currentRow);
    // log('lastPrediction: ', lastPrediction);
    // log('currentPrediction: ', currentPrediction);
    // log('predicted row: ', predictedRow);
    predictions.unshift(predictedRow)
    lastPrediction = currentPrediction;
  }
  return predictions;
}

function getProcessedHistory(historyRow){
  const calculatedRows = [historyRow];
  while(calculatedRows[calculatedRows.length - 1].some(val => val !== 0)){
    calculatedRows.push(getNextRow(calculatedRows[calculatedRows.length - 1]));
  }
  return calculatedRows;
}

function getPredictedSums(processedRow){
  // log('processedRow: ', processedRow);
  // return processedRow.reduce((acc, curr) => acc + curr[curr.length - 1], 0)
  const firstRow = processedRow[0];
  return firstRow[firstRow.length - 1];
}

function getProcessedData(organizedData){
  const processedHistory = organizedData.map(row => {
    const processed = getProcessedHistory(row);
    const predicted = getPredictedHistory(processed);
    const predictedSums = getPredictedSums(predicted);
    // log('processed: ', processed);
    // log('predicted: ', predicted);
    return {
      processed,
      predicted,
      predictedSums
    }
  });
  return processedHistory;
}

function getFinalAnswer(processedData){
  return processedData.reduce((acc, curr) => acc + curr.predictedSums, 0);
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const processedData = getProcessedData(organizedData);
  console.log('processedData: ', processedData)

  const finalAnswer = getFinalAnswer(processedData);
  console.log('finalAnswer: ', finalAnswer);
});