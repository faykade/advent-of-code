const fs = require('node:fs');

const NORTH_CONNECTING = ['S', '|', 'L', 'J'];
const SOUTH_CONNECTING = ['S', '|', '7', 'F'];
const EAST_CONNECTING = ['S', '-', 'L', 'F'];
const WEST_CONNECTING = ['S', '-', 'J', '7'];

function log(...args) {
  console.log(...args);
}

function getOrganizedData(parsedData) {
  return parsedData.map((val) => val.split(''));
}

function getStartingLocation(organizedData) {
  let x = 0;
  let y = 0;
  let found = false;
  let currentRow = [];
  for (let i = 0; i < organizedData.length && !found; i++) {
    currentRow = organizedData[i];
    x = currentRow.indexOf('S');
    if (x >= 0) {
      found = true;
      y = i;
    }
  }
  return {
    x,
    y,
  };
}

function getAdjacentNodes(startingNode, graph) {
  const { x, y } = startingNode;
  const lengthY = graph.length;
  const lengthX = graph[0].length;
  const adjacentNodes = {};
  // top - right - bottom - left
  if (y > 0) {
    adjacentNodes.top = { x, y: y - 1 };
  }
  if (x < lengthX - 1) {
    adjacentNodes.right = { x: x + 1, y };
  }
  if (y < lengthY - 1) {
    adjacentNodes.bottom = { x, y: y + 1 };
  }
  if (x > 0) {
    adjacentNodes.left = { x: x - 1, y };
  }
  return adjacentNodes;
}

function isDirectionallyConnected(node, directionalMatches, graph) {
  const { x, y } = node;
  const char = graph[y][x];
  return directionalMatches.includes(char);
}

function getConnectingNodes(startingNode, adjacentNodes, graph) {
  const connectedNodes = {};
  if (
    adjacentNodes.top &&
    isDirectionallyConnected(startingNode, NORTH_CONNECTING, graph) &&
    isDirectionallyConnected(adjacentNodes.top, SOUTH_CONNECTING, graph)
  ) {
    connectedNodes.top = adjacentNodes.top;
  }
  if (
    adjacentNodes.right &&
    isDirectionallyConnected(startingNode, EAST_CONNECTING, graph) &&
    isDirectionallyConnected(adjacentNodes.right, WEST_CONNECTING, graph)
  ) {
    connectedNodes.right = adjacentNodes.right;
  }
  if (
    adjacentNodes.bottom &&
    isDirectionallyConnected(startingNode, SOUTH_CONNECTING, graph) &&
    isDirectionallyConnected(adjacentNodes.bottom, NORTH_CONNECTING, graph)
  ) {
    connectedNodes.bottom = adjacentNodes.bottom;
  }
  if (
    adjacentNodes.left &&
    isDirectionallyConnected(startingNode, WEST_CONNECTING, graph) &&
    isDirectionallyConnected(adjacentNodes.left, EAST_CONNECTING, graph)
  ) {
    connectedNodes.left = adjacentNodes.left;
  }
  return connectedNodes;
}

function isNodeVisited(node, visitedNodes) {
  return visitedNodes.some((visitedNode) => visitedNode.x === node.x && visitedNode.y === node.y);
}

function getProcessedData(organizedData) {
  const startingNode = getStartingLocation(organizedData);
  const visited = [];
  const toVisitStack = [startingNode];
  let currentNode;
  let connectingNodes = [];
  while (toVisitStack && toVisitStack.length > 0) {
    currentNode = toVisitStack.pop();
    if (currentNode && !isNodeVisited(currentNode, visited)) {
      visited.push(currentNode);
      connectingNodes = Object.values(
        getConnectingNodes(currentNode, getAdjacentNodes(currentNode, organizedData), organizedData),
      );
      // log('connectingNodes: ', connectingNodes);
      connectingNodes = connectingNodes.filter((node) => !isNodeVisited(node, visited));
      if (connectingNodes && connectingNodes.length > 0) {
        toVisitStack.push(...connectingNodes);
      }

      // log('visited: ', visited);
      // log('toVisit: ', toVisitStack);
    }
  }

  log('visited: ', visited);

  return visited;
}

function getFinalAnswer(processedData) {
  return processedData.length / 2;
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const processedData = getProcessedData(organizedData);
  console.log('processedData: ', processedData);

  const finalAnswer = getFinalAnswer(processedData);
  console.log('finalAnswer: ', finalAnswer);
});
