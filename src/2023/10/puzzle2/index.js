const fs = require("node:fs");

function log(...args) {
  console.log(...args);
}

function getOrganizedData(parsedData) {
  return parsedData;
}

function getProcessedData(organizedData) {
  return organizedData;
}

function getFinalAnswer(processedData) {
  return processedData;
}

fs.readFile(`${__dirname}/input.txt`, "utf8", (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split("\r\n");
  console.log("parsedData: ", parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log("organizedData: ", organizedData);

  const processedData = getProcessedData(organizedData);
  console.log("processedData: ", processedData);

  const finalAnswer = getFinalAnswer(processedData);
  console.log("finalAnswer: ", finalAnswer);
});
