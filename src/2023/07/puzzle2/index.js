const fs = require('node:fs');

const CARDS_IN_STRENGTH_ORDER = ['J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A'];
const HAND_TYPES = ['5kind', '4kind', 'full', '3kind', '2pair', '1pair', 'high'];

function log(...args){
  console.log(...args);
}

function getSortedHand(hand) {
  return hand.split('').sort().join('');;
}

function getHandType(hand){
  const sortedHand = getSortedHand(hand);
  const brokenDownCounts = sortedHand.split('').reduce((acc, curr) => ({
    ...acc,
    [curr]: acc[curr] ? acc[curr] + 1 : 1
  }), {});

  const { 'J': wilds = 0, ...nonWilds } = brokenDownCounts; 

  let countBest = 0;
  let countSecondBest = 0;
  Object.values(nonWilds).forEach(numCards => {
    if(numCards > countBest){
      countSecondBest = countBest;
      countBest = numCards;
    } else if(numCards > countSecondBest){
      countSecondBest = numCards;
    }
  });
  
  if(wilds){
    countBest += wilds;
  }

  if(countBest === 5){
    return '5kind';
  } else if(countBest === 4){
    return '4kind';
  } else if(countBest === 3 && countSecondBest === 2){
    return 'full';
  } else if(countBest === 3){
    return '3kind'
  } else if(countBest === 2 && countSecondBest === 2){
    return '2pair'
  } else if(countBest === 2){
    return '1pair';
  } else {
    return 'high';
  }
}

function getTypedHands(organizedData){
  return organizedData.map(data => ({
    ...data,
    sortedHand: getSortedHand(data.hand),
    type: getHandType(data.hand)
  }));
}

function getGroupedHands(typedHands){
  const groupedHands = [];
  HAND_TYPES.forEach(handType => {
    groupedHands.push(typedHands.filter(hand => hand.type === handType));
  })

  return groupedHands;
}

function getCardStrength(card){
  return CARDS_IN_STRENGTH_ORDER.indexOf(card);
}

function getSortedHandFunction (hand1, hand2){
  let diff = 0;
  for(let i = 0; i < hand1.length && !diff; i++){
    diff = getCardStrength(hand2[i]) - getCardStrength(hand1[i]);
  }
  return diff;
}

function getSortedGroup(handGroup){
  return handGroup.sort((hand1, hand2) => getSortedHandFunction(hand1.hand, hand2.hand));
}

function getSortedHands(typedHands){
  const groupedHands = getGroupedHands(typedHands);
  const sortedHands = groupedHands.map(getSortedGroup);
  log('sortedHands: ', sortedHands);
  const singleListSorted = sortedHands.reduce((acc, curr) => [...acc, ...curr], [])
  log('singleListSorted: ', singleListSorted);
  return singleListSorted;
}

function getProcessedData(organizedData){
  const typedHands = getTypedHands(organizedData);
  const sortedHands = getSortedHands(typedHands);
  return sortedHands;
}

function getOrganizedData(parsedData) {
  return parsedData.map(val => {
    const [hand, bid] = val.split(' ');
    return {
      hand,
      bid: parseInt(bid)
    }
  });
}

function getFinalAnswer(processedData){
  const copy = [...processedData];
  const reversed = copy.reverse().map((data, index) => ({
    ...data,
    rank: index + 1
  }))
  log(reversed);
  const finalAnswer = reversed.reduce((acc, curr) => acc + (curr.rank * curr.bid), 0);
  return finalAnswer;
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const processedData = getProcessedData(organizedData);
  console.log('processedData: ', processedData)

  const finalAnswer = getFinalAnswer(processedData);
  console.log('finalAnswer: ', finalAnswer);
});

