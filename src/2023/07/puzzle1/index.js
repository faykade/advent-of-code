const fs = require('node:fs');

const FIVE_KIND_REGEX_STR = getRegex(5);
const FOUR_KIND_REGEX_STR = getRegex(4);
const FULL_HOUSE_REGEX_STR = `${getRegex(3)}${getRegex(2)}|${getRegex(2)}${getRegex(3)}`;
const THREE_KIND_REGEX_STR = getRegex(3);
const TWO_PAIR_REGEX_STR = `${getRegex(2)}.*${getRegex(2)}`;
const ONE_PAIR_REGEX_STR = getRegex(2);
const HIGH_CARD_REGEX_STR = getRegex(1);

const FIVE_KIND_REGEX = new RegExp(FIVE_KIND_REGEX_STR);
const FOUR_KIND_REGEX = new RegExp(FOUR_KIND_REGEX_STR);
const FULL_HOUSE_REGEX = new RegExp(FULL_HOUSE_REGEX_STR);
const THREE_KIND_REGEX = new RegExp(THREE_KIND_REGEX_STR);
const TWO_PAIR_REGEX = new RegExp(TWO_PAIR_REGEX_STR);
const ONE_PAIR_REGEX = new RegExp(ONE_PAIR_REGEX_STR);
const HIGH_CARD_REGEX = new RegExp(HIGH_CARD_REGEX_STR);

function getNumbersRegex(count){
  const nums = Array.from(Array(10).keys());
  const str = `${nums.join(`{${count}}|`)}{${count}}`;
  return str;
}

function getRegex(count){
  const str = `(A{${count}}|K{${count}}|Q{${count}}|J{${count}}|T{${count}}|${getNumbersRegex(count)})`;
  return str;
}

function log(...args){
  console.log(...args);
}

function getSortedHand(hand) {
  return hand.split('').sort().join('');;
}

function getHandTypes(){
  const priority = [{
    regex: FIVE_KIND_REGEX,
    key: '5kind',
    str: FIVE_KIND_REGEX_STR
  },
  {
    regex: FOUR_KIND_REGEX,
    key: '4kind',
    str: FOUR_KIND_REGEX_STR
  },
  {
    regex: FULL_HOUSE_REGEX,
    key: 'full',
    str: FULL_HOUSE_REGEX_STR
  },
  {
    regex: THREE_KIND_REGEX,
    key: '3kind',
    str: THREE_KIND_REGEX_STR
  },
  {
    regex: TWO_PAIR_REGEX,
    key: '2pair',
    str: TWO_PAIR_REGEX_STR
  },
  {
    regex: ONE_PAIR_REGEX,
    key: '1pair',
    str: ONE_PAIR_REGEX_STR
  },
  {
    regex: HIGH_CARD_REGEX,
    key: 'high',
    str: HIGH_CARD_REGEX_STR
  }]

  return priority;
}

function getHandType(hand, handTypes){
  const sortedHand = getSortedHand(hand);
  let type = 'high';
  for(let i = 0, found = false; i < handTypes.length && !found; i ++){
    const handType = handTypes[i]
    if(sortedHand.match(handType.regex)){
      found = true;
      type = handType.key;
    }
  }

  return type;
}

function getTypedHands(organizedData){
  const handTypes = getHandTypes();
  // log(handTypes);
  return organizedData.map(data => ({
    ...data,
    sortedHand: getSortedHand(data.hand),
    type: getHandType(data.hand, handTypes)
  }));
}

function getGroupedHands(typedHands){
  const handTypes = getHandTypes();
  const groupedHands = [];
  handTypes.forEach(handType => {
    groupedHands.push(typedHands.filter(hand => hand.type === handType.key));
  })

  return groupedHands;
}

function getCardValue(card){
  const charMap = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10
  }
  return charMap[card] ? charMap[card] : parseInt(card);
}

function getSortedHandFunction (hand1, hand2){
  let diff = 0;
  for(let i = 0; i < hand1.length && !diff; i++){
    diff = getCardValue(hand2[i]) - getCardValue(hand1[i]);
  }
  return diff;
}

function getSortedGroup(handGroup){
  return handGroup.sort((hand1, hand2) => getSortedHandFunction(hand1.hand, hand2.hand));
}

function getSortedHands(typedHands){
  const groupedHands = getGroupedHands(typedHands);
  const sortedHands = groupedHands.map(getSortedGroup);
  log('sortedHands: ', sortedHands);
  const singleListSorted = sortedHands.reduce((acc, curr) => [...acc, ...curr], [])
  log('singleListSorted: ', singleListSorted);
  return singleListSorted;
}

function getProcessedData(organizedData){
  const typedHands = getTypedHands(organizedData);
  const sortedHands = getSortedHands(typedHands);
  return sortedHands;
}

function getOrganizedData(parsedData) {
  return parsedData.map(val => {
    const [hand, bid] = val.split(' ');
    return {
      hand,
      bid: parseInt(bid)
    }
  });
}

function getFinalAnswer(processedData){
  const copy = [...processedData];
  const reversed = copy.reverse().map((data, index) => ({
    ...data,
    rank: index + 1
  }))
  log(reversed);
  const finalAnswer = reversed.reduce((acc, curr) => acc + (curr.rank * curr.bid), 0);
  return finalAnswer;
}

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, fileData) => {
  if (err) {
    console.error(err);
    return;
  }

  const parsedData = fileData.split('\r\n');
  console.log('parsedData: ', parsedData);

  const organizedData = getOrganizedData(parsedData);
  console.log('organizedData: ', organizedData);

  const processedData = getProcessedData(organizedData);
  console.log('processedData: ', processedData)

  const finalAnswer = getFinalAnswer(processedData);
  console.log('finalAnswer: ', finalAnswer);
});